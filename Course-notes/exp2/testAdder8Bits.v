module testAdder8Bits(clk,rst,a,b,cin,cout,sum);
input clk,rst,cin;
input[7:0] a,b;
output  reg cout;
output reg[7:0] sum;
reg[8:0] tempVal;

always @(posedge clk)
	begin
		if (rst) tempVal<=0;
		else
			begin
				tempVal<=a+b+cin; 
			end
	end
always @(negedge clk)
	begin
		{cout,sum} <= tempVal;
	end

endmodule

//1、使用VerilogHDL编写一个参数定义的全加器
module FullAdder(oSum,ina,inb,clk,reset);
parameter MSB=31,LSB=0; 
input clk,reset;
input[MSB:LSB] ina,inb;
output reg[MSB+1:LSB] oSum;

always @(posedge clk)
begin
    if (reset) oSum <= 0;
    else
        begin
            oSum<=ina+inb;
        end
end

endmodule

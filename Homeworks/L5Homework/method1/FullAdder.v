//1、使用VerilogHDL编写一个参数定义的全加器
module FullAdder(oSum,ina,inb);
parameter MSB=31,LSB=0; 
input[MSB:LSB] ina,inb;
output[MSB+1:LSB] oSum;

assign oSum=ina+inb; 

endmodule

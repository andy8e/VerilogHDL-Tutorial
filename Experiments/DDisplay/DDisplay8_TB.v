// Copyright (C) 2019  Intel Corporation. All rights reserved.
// Your use of Intel Corporation's design tools, logic functions 
// and other software and tools, and any partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Intel Program License 
// Subscription Agreement, the Intel Quartus Prime License Agreement,
// the Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is for
// the sole purpose of programming logic devices manufactured by
// Intel and sold by Intel or its authorized distributors.  Please
// refer to the applicable agreement for further details, at
// https://fpgasoftware.intel.com/eula.

// *****************************************************************************
// This file contains a Verilog test bench template that is freely editable to  
// suit user's needs .Comments are provided in each section to help the user    
// fill out necessary details.                                                  
// *****************************************************************************
// Generated on "06/10/2020 11:21:29"
                                                                                
// Verilog Test Bench template for design : DDisplay8
// 
// Simulation tool : ModelSim-Altera (Verilog)
// 

`timescale 1 ns/ 1 ps
module DDisplay8_vlg_tst();
// constants                                           
// general purpose registers
reg eachvec;
// test vector input registers
reg [3:0] D0;
reg [3:0] D1;
reg [3:0] D2;
reg [3:0] D3;
reg [3:0] D4;
reg [3:0] D5;
reg [3:0] D6;
reg [3:0] D7;
reg clk;
reg rst;
// wires                                               
wire [6:0]  seg;
wire [7:0]  sel;

// assign statements (if any)                          
DDisplay8 i1 (
// port map - connection between master ports and signals/registers   
	.D0(D0),
	.D1(D1),
	.D2(D2),
	.D3(D3),
	.D4(D4),
	.D5(D5),
	.D6(D6),
	.D7(D7),
	.clk(clk),
	.rst(rst),
	.seg(seg),
	.sel(sel)
);
initial                                                
begin                                                  
// code that executes only once                        
// insert code here --> begin
	D0=0;
   D1=0;
	D2=0;
   D3=0;
	D4=0;
   D5=0;
	D6=0;
   D7=0;
   clk=0;
   rst=1;
   #40 rst=0;
   #10000 $stop;                                                    
// --> end                                             
$display("Running testbench");                       
end

always
begin
	#20 clk=~clk;
end

always
begin
	#200 
	D0=D0+1;if (D0>9) D0=0;
	D1=D1+2;if (D1>9) D1=0;
	D2=D2+1;if (D2>9) D2=0;
	D3=D3+2;if (D3>9) D3=0;
	D4=D4+1;if (D4>9) D4=0;
	D5=D5+2;if (D5>9) D5=0;
	D6=D6+1;if (D6>9) D6=0;
	D7=D7+2;if (D7>9) D7=0;
end


                                                    
always                                                 
// optional sensitivity list                           
// @(event1 or event2 or .... eventn)                  
begin                                                  
// code executes for every event on sensitivity list   
// insert code here --> begin                          
                                                       
@eachvec;                                              
// --> end                                             
end                                                    
endmodule


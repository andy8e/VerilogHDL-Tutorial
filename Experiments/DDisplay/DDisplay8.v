module DDisplay8(clk,rst,D0,D1,D2,D3,D4,D5,D6,D7,seg,sel);
input clk,rst;
input[3:0] D0,D1,D2,D3,D4,D5,D6,D7;
output[6:0] seg;
output[7:0] sel;

wire[3:0] disData;

sel8_1 mysel8_1(clk,rst,D0,D1,D2,D3,D4,D5,D6,D7,disData,sel);

decode4_7 mydec(disData,seg);

endmodule

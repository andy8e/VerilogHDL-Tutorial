module cnt(clk,reset,set,data,out);
input clk,reset,set;
input[7:0] data;
output reg[7:0] out;
always @(posedge clk)
	begin
		if (reset) out=0;
		else if (set) out = data;
			 else if (out>=25) out =0;
				  else out=out+1;
	end
endmodule

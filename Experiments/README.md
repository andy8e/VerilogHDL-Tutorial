# 8个小实验例程：

#### 介绍
本目录下包括8个简单的实例

#### 清单
1.  adder：一个二进制加法器实例
2.  mult: 一个乘法器实例
3.  BCD_Adder:一个BCD加法器实例
4.  counter：一个二进制计数器实例
5.  DivFreq：一个分频器实例
6.  seg7dis:一个7段数码管译码显示实例（静态）
7.  DDisplay：一个7段数码管译码显示实例（动态扫描态）
8.  fsm：一个有限状态机实例

#### 参与贡献

1.  以上工作由YouXiaoquan完成



#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

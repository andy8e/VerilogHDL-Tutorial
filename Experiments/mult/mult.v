module mult(outcome,a,b);
parameter SIZE=4;
input[SIZE:1] a,b;
output[2*SIZE:1] outcome;
assign outcome = a*b;

/*
reg[2*SIZE:1] temp_a;
reg[SIZE:1] temp_b;
always @(a or b)
	begin
		outcome = 0;
		temp_a = a;
		temp_b = b;
		repeat (SIZE)
			begin
				if (temp_b[1])
					outcome = outcome+temp_a;
				temp_a = temp_a<<1;
				temp_b = temp_b>>1;
			end
	end
	*/
endmodule

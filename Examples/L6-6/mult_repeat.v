//用repeat实现8位二进制数乘法
module mult_repeat(outcome, a, b);
parameter  size=8;  input[size:1]  a,b;
output[2*size:1]  outcome;
reg[2*size:1]  temp_a,outcome;
reg[size:1]  temp_b;
always @(a or b)
begin  outcome=0; temp_a=a;  temp_b=b;
    repeat(size)    	//repeat语句，size为循环次数
    begin
        if(temp_b[1])   	//如果temp_b的最低位为1，就执行下面的加法
            outcome=outcome +temp_a;
        temp_a=temp_a<<1;    //操作数a左移一位
        temp_b=temp_b>>1;    //操作数b右移一位
    end   end
endmodule

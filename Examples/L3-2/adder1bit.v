module adder1bit(A,B,Cin,Co,Sum);
input A,B,Cin;
output Co,Sum;

assign {Co,Sum} = A+B+Cin;

endmodule

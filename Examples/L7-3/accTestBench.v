// Copyright (C) 2019  Intel Corporation. All rights reserved.
// Your use of Intel Corporation's design tools, logic functions 
// and other software and tools, and any partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Intel Program License 
// Subscription Agreement, the Intel Quartus Prime License Agreement,
// the Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is for
// the sole purpose of programming logic devices manufactured by
// Intel and sold by Intel or its authorized distributors.  Please
// refer to the applicable agreement for further details, at
// https://fpgasoftware.intel.com/eula.

// *****************************************************************************
// This file contains a Verilog test bench template that is freely editable to  
// suit user's needs .Comments are provided in each section to help the user    
// fill out necessary details.                                                  
// *****************************************************************************
// Generated on "04/26/2020 20:30:47"
                                                                                
// Verilog Test Bench template for design : acc
// 
// Simulation tool : ModelSim (Verilog)
// 

`timescale 1 ns/ 1 ps
module acc_vlg_tst();
// constants                                           
// general purpose registers
reg eachvec;
// test vector input registers
reg [7:0] accin;
reg cin;
reg clear;
reg clk;
// wires                                               
wire [7:0]  accout;
wire cout;

// assign statements (if any)                          
acc i1 (
// port map - connection between master ports and signals/registers   
	.accin(accin),
	.accout(accout),
	.cin(cin),
	.clear(clear),
	.clk(clk),
	.cout(cout)
);
initial                                                
begin                                                  
// code that executes only once                        
// insert code here --> begin                          
	accin=0;
	cin=0;
	clear=0;
	clk=0;
	#10	clear=1;
	#10	clear=0;
	#1000 $stop;
// --> end                                             
$display("Running testbench");                       
end                                                    
always
begin
	#10 clk=~clk;
end
always
begin
	#100 cin=~cin;
end
always
begin
	#40 accin=accin+1;
end
always                                                 
// optional sensitivity list                           
// @(event1 or event2 or .... eventn)                  
begin                                                  
// code executes for every event on sensitivity list   
// insert code here --> begin                          
                                                       
@eachvec;                                              
// --> end                                             
end                                                    
endmodule


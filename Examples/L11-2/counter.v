//被测试模块
module counter(qout,reset,clk); //待测试的8位计数器模块
output[7:0] qout;
input clk,reset;
reg[7:0] qout;
always @(posedge clk)
begin 	if(reset)	qout<=0;
    else	qout<=qout+1;
end
endmodule

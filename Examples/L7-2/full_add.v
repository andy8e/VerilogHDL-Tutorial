//用模块例化方式设计的1位全加器顶层设计
module full_add(ain,bin,cin,sum,cout);
input ain,bin,cin; output sum,cout;
wire d,e,f; 	//用于内部连接的节点信号
half_add u1(ain,bin,e,d);
//半加器模块调用，采用位置关联方式
half_add u2(e,cin,sum,f);
or u3(cout,d,f); 	//或门调用
endmodule

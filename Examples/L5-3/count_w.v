//采用参数定义的二进制计数器
module count_w(en,clk,reset,out);
input clk,reset,en;
parameter WIDTH=8; 		//参数定义
output[WIDTH-1:0] out;
reg[WIDTH-1:0] out;
always @(posedge clk or negedge reset)
    if(!reset)   out=0;
    else if(en)  out=out+1;
endmodule

//引入了D触发器的长帧同步时钟的产生
module longframe2(clk,strb);
parameter DELAY=8; input clk;
output strb; reg[7:0] counter; reg temp,strb;
always@(posedge clk)
begin if(counter==255) counter<=0;
    else counter<=counter+1;
end
always@(posedge clk)
begin strb<=temp;end    //引入触发器
always@(counter)
begin if(counter<=(DELAY-1)) temp<=1; else temp<=0; end
endmodule

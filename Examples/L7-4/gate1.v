//门级结构描述
module  gate1(F,A,B,C,D);
input  A,B,C,D;
output  F;
nand(F1,A,B);   //调用门元件
and(F2,B,C,D);
or(F,F1,F2);
endmodule

//n为5反馈系数Ci＝(45)8的m序列发生器
// the generation poly is 1+x**3+x**5
module m_sequence
       (input clr,clk,
        output reg m_out);
reg[4:0] shift_reg;
always @(posedge clk, negedge clr)
begin
    if(~clr) begin shift_reg<=5'b00001; end
    else begin
        shift_reg[0] <= shift_reg[2] ^ shift_reg[4];
        shift_reg[4:1]<=shift_reg[3:0];
        m_out <= shift_reg[4];  end
end
endmodule

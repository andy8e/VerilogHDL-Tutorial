//采用参数定义的约翰逊计数器
module johnson_w(clk,clr,qout);
input clk,clr;
parameter WIDTH=8; 		//参数定义
output reg[(WIDTH-1):0] qout;
always @(posedge clk or posedge clr)
begin
    if(clr) qout<=0;
    else
    begin
        qout<=qout<<1;
        qout[0]<=~qout[width-1];
    end
end
endmodule

//自动比较mux4_1a、mux4_1b、mux4_1c三个模块的输入输出是否一致
module mux4_1(out,in1,in2,in3,in4,s0,s1);
input in1,in2,in3,in4,s0,s1;
output reg out;

wire outa,outb,outc;

mux4_1a mux41a(outa,in1,in2,in3,in4,s0,s1);
mux4_1b mux41b(outb,in1,in2,in3,in4,s0,s1);
mux4_1c mux41c(outc,in1,in2,in3,in4,s0,s1);

always @(outa, outb, outc)
	case({outa, outb, outc})
		3'b000:out<=1'b0;
		3'b001:out<=1'b1;
		3'b010:out<=1'b1;
		3'b011:out<=1'b1;
		3'b100:out<=1'b1;
		3'b101:out<=1'b1;
		3'b110:out<=1'b1;
		3'b111:out<=1'b0;
		default:out<=1'b1;
	endcase

endmodule

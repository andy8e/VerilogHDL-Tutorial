//5.5半整数分频源代码
module fdiv5_5(clkin,clr,clkout);
input clkin,clr; output reg clkout;
reg clk1; wire clk2; integer count;
xor xor1(clk2,clkin,clk1); 			//异或门
always@(posedge clkout or negedge clr)  	//2分频器
begin if(~clr) begin clk1<=1'b0; end
    else clk1<=~clk1;
end
always@(posedge clk2 or negedge clr) 		//模5分频器
    begin  if(~clr)
    begin 	count<=0; clkout<=1'b0; end
    else if(count==5)	  	//要改变分频器的模，只需改变count的值
    begin	count<=0; clkout<=1'b1; end
    else  begin  count<=count+1; clkout<=1'b0;  end
end
endmodule

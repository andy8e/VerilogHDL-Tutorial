//8.1小数分频源代码
module fdiv8_1(clk_in,rst,clk_out);
input clk_in,rst; output reg clk_out;
reg[3:0] cnt1,cnt2;			//cnt1计分频的次数
always@(posedge clk_in or posedge rst)
begin  if(rst)  begin cnt1<=0; cnt2<=0; clk_out<=0;  end
    else if(cnt1<9)			//9次8分频
    begin
        if(cnt2<7) begin cnt2<=cnt2+1; clk_out<=0;  end
        else begin cnt2<=0; cnt1<=cnt1+1; clk_out<=1;  end
    end
    else  begin 			//1次9分频
        if(cnt2<8) begin  cnt2<=cnt2+1;  clk_out<=0;  end
        else       begin  cnt2<=0; cnt1<=0; clk_out<=1;  end
    end
end
endmodule

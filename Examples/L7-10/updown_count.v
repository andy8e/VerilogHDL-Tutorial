//可变模加法/减法计数器
module updown_count(d,clk,clear,load,up_down,qd);
input clk,clear,load,up_down;
input[7:0] d;
output[7:0] qd;
reg[7:0] cnt;
assign qd=cnt;
always @(posedge clk)
begin
    if(!clear)  	cnt<=8'h00;
    //同步清0，低电平有效
    else if(load)  		cnt<=d;	//同步预置
    else if(up_down)  	cnt<=cnt+1;	//加法计数
    else  			cnt<=cnt-1;	//减法计数
end
endmodule

module mod5Cnt_Fsm(clk,clr,z,qout);
parameter Q0=3'b000,Q1=3'b001,Q2=3'b010,Q3=3'b011,Q4=3'b100;
input clk,clr;
output reg z;
output[2:0] qout;
reg[2:0] state;
assign qout=state;
always @(posedge clk or posedge clr)
begin
    if(clr)
		state<=Q0;
	else 
	case(state)
		Q0:
			state<=Q1;
		Q1:
			state<=Q2;
		Q2:
			state<=Q3;
		Q3:
			state<=Q4;
		Q4:
			state<=Q0;
		default:
			state<=Q0;
	endcase
end
always @(state)
begin 
	case(state)
		Q4:
			z=1'b1;
		default:z=1'b0;
	endcase
end
endmodule

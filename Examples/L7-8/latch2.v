//带置位/复位端的1位数据锁存器
module latch2(q,d,le,set,reset);
input d,le,set,reset;
output q;
assign q=reset?0:(set? 1:(le?d:q));
endmodule

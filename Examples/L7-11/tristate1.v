//行为描述的三态门
module tristate1(in,en,out);
input in,en;
output reg out;
always @(in or en)
begin
    if(en) out<=in;
    else  out<=1'bz;
end
endmodule

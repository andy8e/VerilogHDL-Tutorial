//数据流描述的三态门
module tristate3(out,in,en);
input in,en;
output out;
assign out=en?in:1‘bz; 			//若en=1，out=in；
//若en=0，out为高阻态
endmodule

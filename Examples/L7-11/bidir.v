//三态双向驱动器
module bidir(y,a,en,b);
input a,en;
output b;
inout y;
assign y=en?a:'bz;
assign b=y;
endmodule

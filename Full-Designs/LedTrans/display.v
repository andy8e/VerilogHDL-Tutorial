module display(reset,clk,dispVal,segData,sel);
input reset,clk;
input[6:0] dispVal;
output reg[6:0] segData;
output reg[1:0] sel;

reg[3:0] shiwei,gewei,temp;

always @(posedge clk)
	begin
		if (reset) begin shiwei<=0;  gewei<=0; end
		else
			begin
				shiwei <= dispVal/10;
				gewei  <= dispVal%10;
				if (sel==2'b01) begin sel<= 2'b10; temp<=shiwei; end
				else begin sel<= 2'b01;  temp<=gewei; end
				case(temp)   //��case����������
				     4'd0:segData=7'b1111110;
				     4'd1:segData=7'b0110000;
				     4'd2:segData=7'b1101101;
				     4'd3:segData=7'b1111001;
				     4'd4:segData=7'b0110011;
				     4'd5:segData=7'b1011011;
				     4'd6:segData=7'b1011111;
				     4'd7:segData=7'b1110000;
				     4'd8:segData=7'b1111111; 
				     4'd9:segData=7'b1111011;
				     default: segData=7'bx;
			    endcase  
			end
	end


endmodule

module counter(reset,clk,setVal,cntVal,done);
//
input reset,clk;
input[6:0] setVal;
output reg[6:0] cntVal;
output reg done;

always @(posedge clk)
	begin
		if (reset) begin cntVal<=0; done<=1; end
		else
			begin
				if (cntVal==0) done<=1;
				else begin cntVal<=cntVal-1; end
				if ((done)) begin cntVal<=setVal; done<=0; end
			end
	end


endmodule

module MiaoBiao_Top(clk,inClr,inPause,seg,sel);
input clk,inClr,inPause;
output[6:0] seg;
output[7:0] sel;

wire clr,pause;
wire clk100Hz,clk150Hz,clk1Hz,clk1_60Hz,clk1_3600Hz,clk1_24HHz;
wire[6:0] cnt100Val,miaoCntVal,fenCntVal,shiCntVal;
wire[3:0] BCD7,BCD6,BCD5,BCD4,BCD3,BCD2,BCD1,BCD0;

debounce clr_debounce(clk100Hz,inClr,clr);
debounce Pause_debounce(clk100Hz,inPause,pause);

freqDiv  myFreq  (clk,       clk100Hz,clk150Hz);//freqDiv(clk,clk100Hz,clk150Hz);
count100 mycnt100(clk100Hz,  clr,     pause,   clk1Hz,       cnt100Val);//count100 (clk,clr,pause,clkout,cntVal);
count60  miaoCnt (clk1Hz,    clr,     pause,   clk1_60Hz,    miaoCntVal);//count60(clk,clr,pause,clkout,cntVal);
count60  fenCnt  (clk1_60Hz, clr,     pause,   clk1_3600Hz,  fenCntVal);
count24  shiCnt  (clk1_3600Hz, clr,   pause,   clk1_24HHz,  shiCntVal);

//HEX2BCD(HexVal,BCD10,BCD1);
HEX2BCD myHex2BCD_BFM (cnt100Val, BCD1,BCD0);
HEX2BCD myHex2BCD_Miao(miaoCntVal,BCD3,BCD2);
HEX2BCD myHex2BCD_Fen (fenCntVal, BCD5,BCD4);
HEX2BCD myHex2BCD_Shi (shiCntVal, BCD7,BCD6);

//module DDisplay8(clk,rst,D0,D1,D2,D3,D4,D5,D6,D7,seg,sel);
DDisplay8 MyDisp(clk150Hz,clr,BCD0,BCD1,BCD2,BCD3,BCD4,BCD5,BCD6,BCD7,seg,sel);


endmodule

module count60(clk,clr,pause,clkout,cntVal);
input clk,clr,pause;
output reg clkout;//
output reg[6:0] cntVal;//

always @(posedge clk)
	begin
		if (clr) cntVal =0;
		if (!pause)
			begin
				cntVal = cntVal+1;
				if (cntVal==60) 
					begin 
						cntVal =0;
						clkout =1;
					end
				else clkout =0;
			end	
	end

endmodule 

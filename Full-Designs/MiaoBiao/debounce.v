module debounce(
    input wire clk,
    input wire key_in,
    output reg key_out
    );

    reg key_TempH,key_TempL;

    always @(posedge clk) 
        begin
            key_TempH<=key_in;
        end

    always @(negedge clk ) 
        begin
            key_TempL<=key_in;
        end

    always @(*) 
        begin
            if (key_TempL==key_TempH) key_out=key_TempH;
        end
endmodule
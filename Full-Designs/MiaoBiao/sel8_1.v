module sel8_1(clk,rst,D0,D1,D2,D3,D4,D5,D6,D7,disData,sel);
input clk,rst;
input[3:0] D0,D1,D2,D3,D4,D5,D6,D7;
output reg[3:0] disData;
output reg[7:0] sel;
reg[2:0] cnt;

always @(posedge clk)
	begin
	   if(rst) cnt<=0;
		else cnt<=cnt+1;
	end

always @(*)
	begin
		case (cnt)
			3'd0:begin sel<=~8'b00000001;disData<=D0; end
			3'd1:begin sel<=~8'b00000010;disData<=D1; end
			3'd2:begin sel<=~8'b00000100;disData<=D2; end
			3'd3:begin sel<=~8'b00001000;disData<=D3; end
			3'd4:begin sel<=~8'b00010000;disData<=D4; end
			3'd5:begin sel<=~8'b00100000;disData<=D5; end
			3'd6:begin sel<=~8'b01000000;disData<=D6; end
			3'd7:begin sel<=~8'b10000000;disData<=D7; end
		   default:begin sel<=8'b00000000;disData<=4'd10; end
		endcase 
	end	
	
endmodule
